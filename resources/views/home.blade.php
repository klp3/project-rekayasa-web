@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-3">

        <button class="btn btn-block btn-primary" onclick="showModal()">Tambah Pertanyaan</button> <br>
        <ul class="list-group">
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Cras justo odio
            <span class="badge badge-primary badge-pill">14</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Dapibus ac facilisis in
            <span class="badge badge-primary badge-pill">2</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Morbi leo risus
            <span class="badge badge-primary badge-pill">1</span>
          </li>
        </ul>
    </div>

    <div class="col-md-9">
      @if(Session::has('tambahAkun'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{session('tambahAkun')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        @if(Session::has('pesanTambah'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{session('pesanTambah')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        @if(Session::has('pesanHapus'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{session('pesanHapus')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        <div class="list-group">
          <a href="#" class="list-group-item list-group-item-action active">
            Pertanyaan Terbaru
          </a>
    @foreach($berita as $data)
    <div class="list-group">
    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
          <h5 class="mb-1">{{$data->judul}}</h5>
          <small>{{$data->waktu}}</small>
        </div>
        <p class="mb-1">{{$data->deskripsi}}</p>
    </a>
       <ul><a onclick="editPertanyaan({{$data->id_berita}})"><button class="btn btn-primary btn-success">Edit</button></a>
        <a href="{{url('/hapus_pertanyaan/')}}/{{$data->id_berita}}"><button class="btn btn-secondary btn-danger">Hapus</button></a></ul>
    </div>
  @endforeach

</div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="top:100px">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myModal">Tambah Pertanyaan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" action="{{url('/post_pertanyaan')}}" method="post">
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <input type="hidden" name="id_berita">
            <div class="form-group">
                <label for="exampleInputEmail1">Judul</label>
                <input type="text" name="judul" class="form-control" placeholder="Judul Pertanyaan">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Deskripsi</label>
                <input type="text" name="deskripsi" class="form-control" placeholder="Deskripsi Pertanyaan">
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
            <input type="submit" class="btn btn-primary"></input>
        </form>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>

<script>
    function showModal(){
        $('#myModal').modal('toggle');
    }

    function editPertanyaan(id){
        $.ajax({
        url : "{{url('/ambil_data')}}/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_berita"]').val(data.id_berita);
            $('[name="judul"]').val(data.judul);
            $('[name="deskripsi"]').val(data.deskripsi);

            $('#myForm').attr('action', "{{url('/update_pertanyaan')}}");
            $('#myModal').modal('show');
        }
        });
    }
</script>

@endsection
