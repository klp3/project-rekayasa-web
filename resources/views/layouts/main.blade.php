<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'WEB FORUM') }}</title>

    <!-- Styles -->
    @section('css')
    <style type="text/css">
      .modal{
        top:100px;
      }

    </style>

    @endsection
   
        <link href="https://bootswatch.com/4/solar/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div id="app">
       <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><img src="{{url('/images/logo.jpg')}}" alt="E-Community Students" width="150px" height="70px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor02">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Profile</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">About</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" placeholder="Search" type="text">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>&nbsp&nbsp&nbsp&nbsp&nbsp

     <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if(Session::has('username'))
                            <li class="dropdown">
                                <a href="{{url('/logout')}}" class="btn btn-success">
                                    {{ session('username') }} (Logout)</span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{url('/logout') }}"> &nbsp Logout</a>
                                    </li>
                                </ul>
                            </li>
                            
                        @else
                            <li><a class="btn btn-success" href="{{ url('/login') }}">Login</a></li>&nbsp&nbsp
                            <li><a class="btn btn-success" href="{{ url('/register') }}">Register</a></li>
                        @endif
                    </ul>
  </div>
</nav>
    <div class="container-fluid"><br>
        @yield('content')
    </div>
    </div>
    <!-- Scripts -->
    
      <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script type="text/javascript">
      $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
      })
    </script>

</body>
</html>
