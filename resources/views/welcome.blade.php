@extends('layouts.main')

@section('content')
<div class="jumbotron">
  <h1 class="display-4">Selamat Datang!</h1>
  <p class="lead">Ini adalah website forum yang dirancang untuk memfasilitasi mahasiswa-mahasiswi Riau yang kuliah di Yogyakarta.</p>
  <hr class="my-2">
  <p>Gabung bersama kami sekarang juga dan dapatkan informasi penting seputar komunitas mahasiswa Riau yang ada di Yogyakarta.</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="#" role="button">Pelajari Lebih Lanjut</a>
  </p>
</div>

@endsection
