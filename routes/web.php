<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'MasterController@home');
Route::get('/login', [ 'as' => 'login', 'uses' => 'MasterController@login']);

Route::get('/register', 'MasterController@register');
Route::post('/do_register', 'MasterController@do_register');
Route::post('/post_pertanyaan', 'MasterController@post_pertanyaan');
Route::get('/ambil_data/{id}', 'MasterController@ambil_data');
Route::post('/update_pertanyaan', 'MasterController@update_pertanyaan');
Route::get('/hapus_pertanyaan/{id}', 'MasterController@hapus_pertanyaan');
Route::post('/do_login', 'MasterController@do_login');
Route::get('/logout', 'MasterController@logout');
