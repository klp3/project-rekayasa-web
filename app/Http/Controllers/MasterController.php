<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use session;

class MasterController extends Controller
{

    public function home()
    {
        $data['berita'] = DB::table('berita')->get();
        return view('home',$data);
    }

    public function login(){
    	return view('login');
    }

    public function do_login(Request $request){
        $username = $request->email;
        $password = $request->password;

        $data= DB::table('users')->where('email',$username)->where('password',md5($password))->get();

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die;

        $x=0;
        foreach ($data as $datas) {
            $x++;
        }

        if($x==1){
            session(['username' => $username]);
            session(['status' => "Anda berhasil masuk"]);
            return redirect ('/home');
        }else{
            return redirect ('/login');
        }
    }

    public function register(){
        return view('register');
    }

    public function do_register(Request $request){

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'password' => md5($request->password),
        );

        DB::table('users')->insert($data);
        session()->flash('tambahAkun', 'Akun berhasil ditambahkan');
        session(['username' => $request->name]);

        return redirect('/home');
    }

    public function logout(Request $request){
        
        $request->session()->flush();

        return redirect('/home');
    }

    public function post_pertanyaan(Request $request){
        $data = array(
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
        );

        DB::table('berita')->insert($data);

        session()->flash('pesanTambah', 'Berita berhasil ditambahkan');

        return redirect('/home');   
    }

    public function ambil_data($id){
        $data['berita'] = DB::table('berita')->where('id_berita',$id)->get();

        return json_encode($data['berita'][0]);
    }

    public function update_pertanyaan(Request $request){
         $data = array(
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
        );

        DB::table('berita')->where('id_berita',$request->id_berita)->update($data);

        session()->flash('pesanEdit', 'Berita berhasil diupdate');

        return redirect('/home');   
    }

    public function hapus_pertanyaan($id){

        DB::table('berita')->where('id_berita',$id)->delete();

        session()->flash('pesanHapus', 'Berita berhasil dihapus'); 

        return redirect('/home');
    }
}
